import pandas as pd
import warnings
warnings.filterwarnings("ignore", message="numpy.dtype size changed")
warnings.filterwarnings("ignore", message="numpy.ufunc size changed")

columnNames = ['Frequency', 'File Size', 'Threads', 'Test', 'Record Size', 'Specific Threads', 'Iteration', 'Machine', 'Shielding', 'Kernel Shielding']

numberOfRepeats = 20 #This is the number of times a test with the same parameters is repeated

#The following are all the variables that will not change during the testing

rcu = 128
freq = 0
machines = [
    #"RyzenMint"
    #"m01",
    #"m02",
    #"m03",
    #"m04",
    #"m05",
    #"m06",
    #"m07",
    #"m08",
    #"m09",
    "m10",
    "m11",
    "m12"
    #"m13",
    #"m14",
    #"m15",
    #"m16",
    #"m17",
    #"m18",
    #"margo",
    #"edith",
    #"agnes"
    ]

# The following are all the variables that do change during the testing. They are iterated through to make sure all the varieties are tried
f_size = [200000]
r_size = [256]
threads = [8]
shielding = [1, 0]
kernelshielding = [0] # this controls whether the "-k" option is on or off for the cpuset, or cset command. 
# the "-k" option controls whether or not the kernel is allowed to run on the shielded threads, which may affect
# IO-related performance and variability

# the specific threads need to be delimited with semicolons, not commas, because it is stored in a CSV (comma separated values) file, and that will mess with the CSV
specificthreads = ["0;2;4;6;8;10;12;14"]
mode = ['0', '1', '2']


rowList = list() # rowList stores all the dictionaries that hold each individual row
for currentMachine in machines:
    for currentShielding in shielding:
        for currentKernelShielding in kernelshielding:
            if(currentShielding == 0 and currentKernelShielding == 1):
                # if the pandas creator is attempting to turn on kernel shielding when
                # cpu shielding is off, just skip that run, don't add it to the document
                continue
            for fsize in f_size:
                for rsize in r_size:
                    if(not rsize > fsize):
                        # if the size of the records is smaller than the size of the files being read, continue on
                        for currentMode in mode:
                            for specificThreadsNumbers in specificthreads:
                                for numberOfThreads in threads:
                                    for iteration in range(numberOfRepeats + 1):
                                        rowList.append({'Frequency':freq, 'File Size':fsize, 'Record Size':rsize, 'Threads':numberOfThreads, 'Specific Threads':specificThreadsNumbers,
                                'Test':currentMode, 'Iteration':iteration, 'Machine':currentMachine, 'Shielding':currentShielding, 'Kernel Shielding':currentKernelShielding})

benchmarks = pd.DataFrame(rowList, columns = columnNames)
benchmarks.to_csv('iozonetests.csv')
