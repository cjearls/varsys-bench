import os, sys, re

if(len(sys.argv) < 2):
    filePath = input("Type the path to the files: ")
else:
    filePath = os.path.dirname(os.path.realpath(__file__)) + '/' + sys.argv[1]

columns = "Media,Frequency,FileSize,RecordSize,Threads,Iteration,SpecificThreads,Machine,Test,Shielding,KernelShielding,InitialWriteThroughput,RewriteThroughput,ReadThroughput,RereadThroughput,RandomReadThroughput,RandomWriteThroughput"
with open("IOZoneParsed.csv","w") as outputFile:
    outputFile.write(columns+"\n")
    for fileNumber, fileName in enumerate(os.listdir(filePath)):
        # Use regular expression of "Children see throughput for ... = " to extract the throughput
        line = ""
        for header in fileName.split("-"):
            if ":" not in header:
                line += header + ','
            else:
                line += header.split(':')[1] + ','
        with open(filePath + '/' + fileName, "r") as inputFile:
            inputFileContents = inputFile.readlines() 
            # set the variables to hold throughput to empty strings so if they
            # aren't present, it doesn't mess up the string
            initialWrite = ''
            rewrite = ''
            read = ''
            reread = ''
            randomread = ''
            randomwrite = ''
            for fileLine in inputFileContents:
                # this loop checks for any of the known tests and records the results
                if('"  Initial write "' in fileLine):
                    initialWrite = fileLine.split(" ")[len(fileLine.split(" "))-2]
                elif('"        Rewrite "' in fileLine):
                    rewrite = fileLine.split(" ")[len(fileLine.split(" "))-2]
                elif('"           Read "' in fileLine):
                    read = fileLine.split(" ")[len(fileLine.split(" "))-2]
                elif('"        Re-read "' in fileLine):
                    reread = fileLine.split(" ")[len(fileLine.split(" "))-2]
                elif("    Random read " in fileLine):
                    randomread = fileLine.split(" ")[len(fileLine.split(" "))-2]
                elif('"   Random write "' in fileLine):
                    randomwrite = fileLine.split(" ")[len(fileLine.split(" "))-2]
            try:
                line += str(initialWrite + ',' + rewrite + ',' + read + ',' + reread + ',' + randomread + ',' + randomwrite).replace('\n', '') + "\n"
                if(all(i == '' for i in [initialWrite, rewrite, read, reread, randomread, randomwrite])):
                    print("The file named '" + fileName + "' did not have any data to be read")
            except:
                print("That string could not be converted correctly")
        outputFile.write(line)
        
