import statistics
import math


def sortBySpecificThreads(line):
    return(line.split(',')[6])

def sortByFileSize(line):
    return(int(line.split(',')[2]))

def sortByRecordSize(line):
    return(int(line.split(',')[3]))

def sortByTest(line):
    return(line.split(',')[8])

def sortByNumThreads(line):
    return(int(line.split(',')[4]))

def sortByShielded(line):
    return(int(line.split(',')[9]))
    
def sortByKernelShielded(line):
    return(int(line.split(',')[10]))

def sortByIterations(line):
    return(int(line.split(',')[5]))


with open("IOZoneParsed.csv", 'r') as readFile:
    document = readFile.readlines()


documentCSVSorted = sorted(sorted(sorted(sorted(sorted(sorted(sorted(sorted(document[1:], key=sortByKernelShielded, reverse=0), key=sortByShielded, reverse=0), key=sortByIterations, reverse=0), key=sortByFileSize, reverse=0), key=sortBySpecificThreads, reverse=0), key=sortByRecordSize, reverse=0), key=sortByTest, reverse=0), key=sortByNumThreads, reverse=0)

throughputsDict = dict()

for element in documentCSVSorted:
    separatedElement = element.split(',')
    independentVariables = (separatedElement[6], separatedElement[2], separatedElement[3], separatedElement[8], separatedElement[4], separatedElement[9], separatedElement[10])
    throughputs = list()
    for number in range(11,17):
        if(separatedElement[number].replace('\n','') == ''):
            throughputs.append(0)
        else:
            throughputs.append(float(separatedElement[number]))
    if independentVariables not in throughputsDict:
        throughputsDict[independentVariables] = list()
    throughputsDict[independentVariables].append(throughputs)

CVList = list()
with open("variationAnalyzed.csv", "w") as outputFile:
    outputFile.write("Specific Threads, File Size, Record Size, Test, Number of Threads, Shielding, Kernel Shielding, AverageInitialWriteThroughput,AverageRewriteThroughput,AverageReadThroughput,AverageRereadThroughput,AverageRandomReadThroughput,AverageRandomWriteThroughput, InitialWriteCV, RewriteCV, ReadCV, RereadCV, RandomReadCV, RandomWriteCV\n")
    for keyCombo in throughputsDict.keys():
        CVs = list() # makes a list of the different CVs of each throughput type
        numberOfThroughputs = 6
        throughputTotals =  [0]*numberOfThroughputs # keeps track of the totals for each type of throughput
        throughputNumbers = [0]*numberOfThroughputs # keeps track of the numbers of throughput statistics for each type of throughput
        throughputAverages = [0]*numberOfThroughputs
        throughputTotalMeanDifferenceSquared = [0]*numberOfThroughputs
        throughputStandardDeviations = [0]*numberOfThroughputs
        throughputCVs = [0]*numberOfThroughputs

        # Find average of each type of throughput
        for datum in throughputsDict[keyCombo]:
            for throughputNumber in range(numberOfThroughputs):
                if(datum[throughputNumber] != 0):
                    throughputTotals[throughputNumber] += datum[throughputNumber]
                    throughputNumbers[throughputNumber] += 1
        for throughputNumber in range(numberOfThroughputs):
            if(throughputNumbers[throughputNumber] != 0):
                throughputAverages[throughputNumber] = throughputTotals[throughputNumber]/throughputNumbers[throughputNumber]
            else:
                throughputAverages[throughputNumber] = ''
        # Find the Variance of each type of throughput
        for datum in throughputsDict[keyCombo]:
            for throughputNumber in range(numberOfThroughputs):
                if(datum[throughputNumber] != 0):
                    throughputTotalMeanDifferenceSquared[throughputNumber] += math.pow(throughputAverages[throughputNumber]-datum[throughputNumber],2)
        for throughputNumber in range(numberOfThroughputs):
            if(throughputNumbers[throughputNumber] != 0):
                throughputStandardDeviations[throughputNumber] = math.pow(throughputTotals[throughputNumber]/throughputNumbers[throughputNumber],.5)
                throughputCVs[throughputNumber] = throughputStandardDeviations[throughputNumber]/throughputAverages[throughputNumber]
            else:
                throughputCVs[throughputNumber] = ''
                    

            #print("The coefficient of variation is: ", CV) # useful for debugging, but not always needed, especially for large sets of data
        CVList.append(CVs)
        for item in range(len(throughputAverages)):
            throughputAverages[item] = str(throughputAverages[item])
            throughputCVs[item] = str(throughputCVs[item])
        outputFile.write(','.join(keyCombo) + ", " + ','.join(throughputAverages) + ", " + ','.join(throughputCVs) + "\n")


#print("Done with sorting. CV average is ", statistics.mean(CVList), " and the coefficient of variation of the data's CV is ", statistics.stdev(CVList)/statistics.mean(CVList))
