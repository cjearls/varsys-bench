#!/usr/bin/env python3

import multiprocessing
import os
import socket
import subprocess
import sys
import time
import timeit


def powers_of_two(start, limit):
    cur, res = start, []
    while (2 ** cur) <= limit:
        res.append(2 ** cur)
        cur += 1
    return res


def set_frequency(frequency, delay):
    ret = subprocess.call(['sudo', 'cpufreq-set', '-f', str(frequency)])
    if ret != 0:
        print("ERROR: failed to set CPU frequency to %d" % frequency)
        exit(-1)

    print("DEBUG: set frequency to %d" % frequency)

    # Allow time for new frequency to settle
    time.sleep(delay)


def run_iozone(outpath, frequency, fsize = '', threads = '', test = '', rsize = '', iteration = '', specificThreads = '', machine = '', shielding = '0', kernelshielding = '0'):
    fileName = (outpath + '/SSD-freq:' + frequency + '-fsize:' + fsize + '-rsize:' + rsize + '-threads:' + threads + '-iteration:'
    + iteration + '-specificThreads:' + specificThreads + '-machine:' + machine + '-test:' + test + '-shielding:' + str(shielding)
    + '-kernelshielding:' + str(kernelshielding))
    if(os.path.exists(fileName)):
        # tests if the file name exists before moving forward and overwriting it
        print("\nThe file " + fileName + " already exists, skipping this particular test\n")
    else:
        if(shielding):
            # if the shielding parameter is present, translates it to a boolean value to be used later in the function
            if(shielding == '1'):
                shielding = True
            elif(shielding == '0'):
                shielding = False
            else:
                print('The shielding field is "' + shielding + '", which is an invalid value. Please set the shielding field to either a 0, to indicate False, or a 1, to indicate True, before continuing')
                exit(-1)

            # also translates the kernelshielding parameter into a boolean for use later
            if(kernelshielding == '1'):
                kernelshielding = True
            elif(kernelshielding == '0'):
                kernelshielding = False
            else:
                print('The kernel shielding field is "' + shielding + '", which is an invalid value. Please set the kernel shielding field to either a 0, to indicate False, or a 1, to indicate True, before continuing')
                exit(-1)

            
            if(shielding):
                '''
                if shielding is desired, makes some checks to be sure that formatting is correct, then sets up the benchmark
                to run shielded by adding to the "cmd" variable before it is used later.
                '''
                if(threads and specificThreads):
                    # checks to be sure that there is a specified number of threads and threads specified to be shielded
                    if(int(threads) == len(specificThreads.split(';'))):
                        # checks to be sure that the number of threads given in the "Threads" parameter and the number of threads in the "Specific Threads" parameter are equal

                        # the '--' on the next line might not initially make sense, but it tells cset that the parameters that follow are for the program that is running, not to be taken as cset parameters
                        cmd = ['sudo', 'cset', 'shield', '--exec', '--']
                        # adding the ability to run specific threads from the input file.
                        # threads from the input file are separated by semicolons instead of commas because the file itself is comma separated
                        # so the semicolons need to be replaced by commas for the purposes of the command, but everything else should be alright
                        cpuShield = ['sudo', 'cset', 'shield', '-k']  # command for shielding the CPU before running the test
                        # changes the shielding command depending on whether or not kernel shielding is desired
                        if(kernelshielding):
                            cpuShield.append('on')
                        else:
                            cpuShield.append('off')
                        cpuShield.append(('--cpu='+specificThreads.replace(";", ",")))

                        cpuUnshield = ['sudo', 'cset', 'shield', '--reset'] # command for unshielding the CPU after the test has been run
                    else:
                        print('The number of threads was found to be "' + str(int(threads)) +
                            '", while the number of threads found in the specific threads parameter was found to be "'
                            + str(len(specificThreads.split(';'))) + '", which does not match. Remember that the specific threads need to be separated by semicolons (;) to be read correctly')
                else:
                    print('The number of threads or the specific threads to be shielded is missing, please supply them and try again')
                    exit(-1)
            else:
                # if shielding is not desired, sets up the necessary variables to allow the benchmark to run
                shielding = False
                cmd = []
        else:
            # if the shielding parameter is not present, it is assumed that the user does not want shielding, so it is not used
            shielding = False

        '''
        Now that shielding has been determined and set up, the rest of the IOZone can be set up to run, based on the other supplied parameters
        '''
        cmd.append('./iozone')
        cmd.append('-M')
        cmd.append('-L')
        cmd.append('64')
        if(fsize):
            cmd.append('-s')
            cmd.append(str(fsize))
        if(threads):
            cmd.append('-t')
            cmd.append(str(threads))
        cmd.append('-i')
        cmd.append('0')
        if(test):
            if(test != '0'):
                # if the write test is already running, no need to rerun it, so checks for that first
                cmd.append('-i')
                cmd.append(str(test))
        if(rsize):
            cmd.append('-r')
            cmd.append(str(rsize))
        cmd.append('-R') # adds more easily scrapable scores at the ends of the completed document
        print("Just so you are aware, the specificThreads functionality is turned off, so it only really works for shielding right now")
        '''
        if(specificThreads):
            cmd.append('-P')
            cmd.append(str(specificThreads.replace(';',',')))
        '''

        # print('DEBUG: - %s' % cmd)
        print('Running ' + fileName)
        if(shielding):
            print("CPU Shield:\n" + str(cpuShield))
        print("IOZone Command:\n" + str(cmd))
        if(shielding):
            print("CPU Unshield:\n" + str(cpuUnshield))

        # opening the file to output the test data
        with open(fileName, 'w') as output:
            if(shielding):
                subprocess.call(cpuShield)
            start_time = timeit.default_timer()
            ret = subprocess.call(cmd, stdout=output, stderr=output) # sets the error and output streams of the iozone command to the designated file and saves the return value to be checked for an error
            if ret != 0:
                print("ERROR: iozone test failed")
                print("It may have failed because you installed iozone with apt-get and you are using the './iozone' command instead of 'iozone...' like you should be, or it may be the other way around, and you installed in a folder, but you're using 'iozone...' instead of './iozone'. This can be changed in the 'cmd' variable in the 'run_iozone' function")
                if(shielding):
                    subprocess.call(cpuUnshield) # even if you exit with an error, it is good practice to stop the current cpu shield
                exit(-1)

            elapsed_time = timeit.default_timer() - start_time
            if(shielding):
                subprocess.call(cpuUnshield)
            output.write("\nElapsed time: %d seconds\n" % elapsed_time)

            if elapsed_time > 3600:
                print("Maximum runtime reached, exiting.")
                sys.exit(0)

def parseCSV(csvName):
    with open(csvName, 'r') as csvFile:
        lines = csvFile.readlines()

        for lineIndex, line in enumerate(lines):
            # removing the newline characters
            lines[lineIndex] = lines[lineIndex].replace('\n', '').replace(' ', '').split(',')
            if(lineIndex == 0):
                # make the first line, the line for the headers, lower case, but leave the other lines alone
                for headerIndex, header in enumerate(lines[lineIndex]):
                    lines[lineIndex][headerIndex] = lines[lineIndex][headerIndex].lower()

        configurationList = list()

        for line in lines[1:]:
            configuration = dict()
            for columnIndex, column in enumerate(line):
                configuration[lines[0][columnIndex]] = column
            configurationList.append(dict(configuration))
        

    return(configurationList)



# Who am I?
MY_HOSTNAME = socket.gethostname().split('.')[0]

# Location to store iozone output.
OUTPUTPATH = "/home/cjearls/benchmarks/%s" % MY_HOSTNAME

# Amount of time to wait for frequency to settle.
FREQUENCY_DELAY_SEC = 2

# Available CPU frequencies.
FREQUENCIES = [
    # 3500000,
    # 3400000,
    # 3200000,
    # 3100000,
    # 2900000,
    2800000,
    # 2600000,
    # 2500000,
    2300000,
    # 2200000,
    2000000,
    # 1900000,
    # 1700000,
    1600000,
    # 1400000,
    1200000]

# The set of hosts that will be assigned a test to run.
HOSTS = ['m01',
         'm02',
         'm03',
         'm04',
         'm05',
         'm06',
         'm07',
         'm08',
         'm09',
         'm10',
         'm11',
         'm12',
         'm13',
         'm14',
         'm15',
         'm16',
         'm17',
         'm18']

'''
# this can be used to change the CPU frequency, but because of how cpufreq-set works, it only changes the frequency and governor of cpu0, the first cpu core, not all of them at once
print("INFO: setting cpu frequency governor to \"userspace\"...")
ret = subprocess.call(['sudo', 'cpufreq-set', '-g', 'userspace'])
if ret != 0:
    print("ERROR: failed to set CPU governor to \"userspace\"")
    exit(-1)
'''
print("Not changing the CPU frequency")

print("INFO: host %s running tests..." % (MY_HOSTNAME))
#print("DEBUG: file size: %dKB" % FILESIZE_KB)
#print("DEBUG: iterations %s" % ITERATIONS)


if not os.path.exists(OUTPUTPATH):
    os.makedirs(OUTPUTPATH)

configurationList = parseCSV('iozonetests.csv')

for configurationIndex, configuration in enumerate(configurationList):
    # the configuration list is enumerated to make it easier for the intelligent assignment of tasks
    # when that is implemented. 

    if(configuration['machine']):
        if(configuration['machine'] == MY_HOSTNAME):
            # the previous two "if" statements make sure that the tests only run on the designated hosts
            if(not int(configuration['recordsize']) > int(configuration['filesize'])):
                # set_frequency(int(configuration['frequency']), FREQUENCY_DELAY_SEC) # this was commented out because I was running the tests without changing the CPU frequency manually, uncomment it to restore functionality
                # if you uncomment this, you also need to uncomment the subprocess.call above that enables the userspace cpu frequency governor for it to work properly
                # also, this is flawed, because it only changes the frequency of cpu0, not all of the frequencies, so don't make that mistake again

                run_iozone(outpath = OUTPUTPATH, frequency = configuration['frequency'],
                    fsize = configuration['filesize'], threads = configuration['threads'], test = configuration['test'],
                    rsize = configuration['recordsize'], specificThreads = configuration['specificthreads'], iteration = configuration['iteration'],
                    machine = configuration['machine'], shielding = configuration['shielding'], kernelshielding = configuration['kernelshielding'])
        else:
            print('The hostname given "' + MY_HOSTNAME)
    else:
        # this is where the intelligent assignement of tests will take place in the future.
        # for now, all the tests should have machine assignments, so this is being foregone in the interest
        # of starting the tests
        print('No machine configuration')

print("INFO: all tests completed, exiting")
